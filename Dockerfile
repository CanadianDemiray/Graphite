FROM node:18-alpine

# Install necessary build tools
RUN apk add --no-cache --virtual .build-deps \
    build-base \
    python3 \
    git \
    && apk add --no-cache curl \
    && curl https://sh.rustup.rs -sSf | sh -s -- -y

# Set environment variables for Rust
ENV PATH="/root/.cargo/bin:${PATH}"

# Install wasm-pack and cargo-watch
RUN cargo install wasm-pack \
    && cargo install cargo-watch

# Set working directory
WORKDIR /app

# Copy package.json and package-lock.json
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the rest of the application code
COPY . .

# Change working directory to frontend
WORKDIR /app/frontend

# Install frontend dependencies
RUN npm install

# Expose the port the app runs on
EXPOSE 8080

# Command to run the application
CMD ["npm", "start"]
